import React from 'react';
import Logout from '../Logout'

const Header = () => {
	return (
		<header>
			<div>
				<h1><a href="/">LiveRadio</a></h1>
			</div>
			<div>
				<div>
					<Logout />
				</div>
			</div>
		</header>
	)
}

export default Header;