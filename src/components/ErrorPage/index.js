import React from 'react'
import test from "../../Images/test.png"

const centerH2 = {
	textAlign: 'center',
	marginTop: '50px'
}

const centerImg = {
	display: 'block',
	margin: '40px auto'
}

const ErrorPage = () => {
	return (
		<div>
			<h2 style={centerH2}>Oups, cette page n'existe pas!</h2>
			<img style={centerImg} src={test} alt="error-page" />
		</div>
	)
}

export default ErrorPage;