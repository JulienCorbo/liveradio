import React, { useContext } from 'react'
import { FirebaseContext } from '../Firebase'
import { Link } from 'react-router-dom'


const Logout = () => {

	const firebase = useContext(FirebaseContext);

	const logout = () => {
		console.log("deco")
		firebase.signoutUser();
	};

	return (
		<div>
			<Link onClick={logout} to="/login" >Deconnexion</Link>
		</div>
	)
}

export default Logout;