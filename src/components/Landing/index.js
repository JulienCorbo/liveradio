import React, { Component } from 'react';
import '../../index.css';
import { Link } from 'react-router-dom'

class App extends Component {
	render () {
		return (
			<main>		
				<div>
					<Link className="button" to ="/signup">Inscription</Link>
					<Link className="button" to ="/login">Connexion</Link>
				</div>
			</main>
		)
	}
}

export default App;