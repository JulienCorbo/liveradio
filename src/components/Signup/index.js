import React, { useState, useContext} from 'react';
import { Link } from  'react-router-dom'
import { FirebaseContext } from '../Firebase'

const Signup = (props) => {

	const firebase = useContext(FirebaseContext);

	const data = {
		pseudo: '',
		email: '',
		password: '',
		confirmPassword: ''
	}

	const [loginData, setLoginData] = useState(data);
	const [error, setError] = useState('');

	const handleChange = e => {
		setLoginData({...loginData, [e.target.id]: e.target.value});
	}

	const handleSubmit = e => {
		e.preventDefault();
		const { email, password } = loginData;
		firebase.signupUser(email, password)
		.then(user =>{
			setLoginData({...data});
			props.history.push('/welcome')
		})
		.catch(error =>{
			setError(error)
		})
	}

	const { pseudo, email, password, confirmPassword } = loginData;

	const btn = pseudo ==='' || email ==='' || password ==='' || password !== confirmPassword
	? <button disabled>Inscription</button> : <button>Inscription</button> 

	//gestion erreurs
	const errorMsg = error !== '' && <span>{error.message}</span>

	return (
		<div>
			<form onSubmit={handleSubmit}>
				{errorMsg}
				<h2>Inscription</h2>
				<div>
					<input onChange={handleChange} value={pseudo} type="text" id="pseudo" required />
					<label htmlFor="pseudo">Pseudo</label>
				</div>

				<div>
					<input onChange={handleChange} value={email} type="email" id="email" required />
					<label htmlFor="email">Email</label>
				</div>

				<div>
					<input onChange={handleChange} value={password} type="password" id="password" required />
					<label htmlFor="password">Mot de passe</label>
				</div>

				<div>
					<input onChange={handleChange} value={confirmPassword} type="password" id="confirmPassword" required />
					<label htmlFor="confirmPassword">Confirmer le Mot de passe</label>
				</div>

				{btn}
			</form>
			<div>
				<Link to="/login">Déjà inscrit? Connectez-vous.</Link>
			</div>
		</div>
	)
}

export default Signup;

