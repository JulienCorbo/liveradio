import React, { useContext } from 'react'
import { FirebaseContext } from '../Firebase'
// import '../../index.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import Header from '../Header'
import Landing from '../Landing'
import Footer from '../Footer'
import Welcome from '../Welcome'
import Login from '../Login'
import Signup from '../Signup'
import ErrorPage from '../ErrorPage'

function App(props) {
	const firebase = useContext(FirebaseContext);
	firebase.auth().onAuthStateChanged(user => {
		if (user) {
			props.history.push('/welcome')
			// save the current user's uid to redux store.

			return (
				<Router>
					<Header />

					<Switch>
						<Route exact path="/" component={Landing} />
						<Route path="/welcome" component={Welcome} />
						<Route path="/login" component={Login} />
						<Route path="/signup" component={Signup} />
						<Route component={ErrorPage} />
					</Switch>

					<Footer />
				</Router>
			);
		} else {
		props.history.push('/')
		}
	})
}

export default App;

