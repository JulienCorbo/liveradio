import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore'

const config = {
  	apiKey: "AIzaSyDzftVHY6aEJT8LDkNzMrbO8zArik-A6yc",
    authDomain: "liveradio-4d790.firebaseapp.com",
    databaseURL: "https://liveradio-4d790.firebaseio.com",
    projectId: "liveradio-4d790",
    storageBucket: "liveradio-4d790.appspot.com",
    messagingSenderId: "838751800758",
    appId: "1:838751800758:web:cafb60f2c58e253752c517",
    measurementId: "G-HTKB303S0D"
}

class Firebase {
    constructor() {
        app.initializeApp(config);
        this.auth = app.auth();
    }

    // inscription
    signupUser = (email, password) => 
    this.auth.createUserWithEmailAndPassword(email, password);

    // Connexion
    loginUser = (email, password) => 
    this.auth.signInWithEmailAndPassword(email, password);

    // Déconnexion
    signoutUser = () => this.auth.signOut();
}

export default Firebase;